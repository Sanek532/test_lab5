package com.bignerdranch.android.dohod

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

private const val EXTRA_ANSWER_IS_TRUE = "com.bignerdranch.android.dohod.rezdohod"

private lateinit var dohod_str: TextView

private  var rezdohod = 0.0

class RezActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rez)

        dohod_str = findViewById(R.id.rez_str)

        rezdohod = intent.getDoubleExtra(EXTRA_ANSWER_IS_TRUE, 0.0)

        dohod_str.text = rezdohod.toString()



    }

    companion object {
        fun newIntent(packageContext: Context, rezdohod: Double): Intent {
            return Intent(packageContext, RezActivity::class.java).apply {
                putExtra(EXTRA_ANSWER_IS_TRUE, rezdohod)
            }
        }
    }

}