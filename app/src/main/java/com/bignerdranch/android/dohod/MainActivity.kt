package com.bignerdranch.android.dohod

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var resButton: Button
    private lateinit var rb_35: RadioButton
    private lateinit var rb_17: RadioButton
    private lateinit var rb_64: RadioButton
    private lateinit var finZatr: EditText
    private lateinit var finRez: EditText

    private var diskont: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        resButton = findViewById(R.id.res)
        rb_17 = findViewById(R.id.rb_17)
        rb_35 = findViewById(R.id.rb_35)
        rb_64 = findViewById(R.id.rb_64)
        finZatr = findViewById(R.id.fin_zatr)
        finRez = findViewById(R.id.fin_rez)



        rb_17.setOnClickListener() {
            diskont = 17.0
        }

        rb_35.setOnClickListener() {
            diskont = 35.0
        }

        rb_64.setOnClickListener() {
            diskont = 64.0
        }

    }

    fun onButtonResClicked(view: View) {
        var rezdohod: Double
        if (diskont == 0.0 || finRez.text.toString() == "" || finZatr.text.toString() == "") {
            sendMessage()
        } else {
            val a = finRez.getText().toString().toDouble()
            val b = finZatr.getText().toString().toDouble()
            if (a - b < 0) {
                sendMessage()
                return
            }
            rezdohod = (a - b) / (1 - diskont / 100)
            val intent = RezActivity.newIntent(this@MainActivity, rezdohod)
            startActivity(intent)
        }
    }

    private fun sendMessage() {
        val messageResId = R.string.incorrect
        Toast.makeText(
            this,
            messageResId,
            Toast.LENGTH_SHORT
        )
            .show()
    }
}